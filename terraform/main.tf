# terraform/main.tf

# -----------------------------------------------------------------------------
# GitLab Sandbox Cloud - Terraform Environment Configuration
# See INSTALL.md for instructions on example usage.
# Ask for help in `#sandbox-cloud-questions`
# -----------------------------------------------------------------------------

# FAQ: Where are all of the scaffolding resources?
# -----------------------------------------------------------------------------
# When Terraform commands are run, all `.tf` files in this directory are
# processed. For clarity and simplicity, the scaffolding resources and
# modules that are included in this environment are in separate files. You
# do not need to make any changes to these files. Simply add the Terraform
# modules or resources that you want to provision below in main.tf. For more
# complex environments, feel free to add additional `.tf` files.

# Terraform Providers
# See terraform/providers.tf

# VPC and Network Configuration
# See terraform/network.tf

# DNS Configuration and Records
# See terraform/dns.tf

# -----------------------------------------------------------------------------
# Add your Terraform modules and/or resources below this line
# -----------------------------------------------------------------------------

# Provision a compute instance
# https://gitlab.com/gitlab-com/sandbox-cloud/terraform-modules/gcp/gcp-compute-instance-tf-module/-/blob/main/README.md
module "experiment_instance" {
  source = "git::https://gitlab.com/gitlab-com/sandbox-cloud/terraform-modules/gcp/gcp-compute-instance-tf-module.git"
  # source = "git::https://gitlab.com/gitlab-com/sandbox-cloud/terraform-modules/gcp/gcp-compute-instance-tf-module.git?ref=0.4.0"

  # Required variables
  gcp_machine_type     = "e2-standard-2"
  gcp_project          = var.gcp_project
  gcp_region           = var.gcp_region
  gcp_region_zone      = var.gcp_region_zone
  gcp_subnetwork       = google_compute_subnetwork.region_subnet.self_link
  instance_description = "Instance for Terraform module experiment"
  instance_name        = "experiment-instance"

  # Optional variables with default values
  disk_boot_size          = "10"
  disk_storage_enabled    = "false"
  disk_storage_mount_path = "/var/opt"
  disk_storage_size       = "100"
  dns_create_record       = "true"
  dns_ttl                 = "300"
  gcp_deletion_protection = "false"
  gcp_dns_zone_name       = data.google_dns_managed_zone.dns_zone.name
  gcp_image               = "ubuntu-1804-lts"
  gcp_network_tags        = ["experiment-instance", "linux-firewall-rule"]

  # Labels for metadata and cost analytics
  labels = var.labels
}



# -----------------------------------------------------------------------------
# Add your Terraform modules and/or resources above this line
# -----------------------------------------------------------------------------
