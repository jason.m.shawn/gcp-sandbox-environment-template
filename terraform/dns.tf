# terraform/dns.tf

# FAQ: How do I get a domain name for my resources?
# -----------------------------------------------------------------------------
# The GitLab Sandbox Cloud provisioning scripts create a subdomain for each
# Terraform environment. This data source fetches the details about the
# subdomain and allows you to create DNS records using resources (see below).
# Any Terraform modules can also use this data source to automatically create
# DNS records for resources in those modules.
# Example: *.jmartin-a1b2c3d4.gcp.gitlabsandbox.cloud

# GCP DNS Zone for Terraform Environment
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/dns_managed_zone
# This is pre-configured by GitLab Sandbox Cloud scripts and is set as an
# CI variable in the GitLab project. Do not make changes to this data source.
data "google_dns_managed_zone" "dns_zone" {
  name = var.gcp_dns_zone_name
}

# -----------------------------------------------------------------------------
# Add custom DNS records below this line
# -----------------------------------------------------------------------------

# Subdomain record for Application Server
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/dns_record_set
# -----------------------------------------------------------------------------
# resource "google_dns_record_set" "gitlab_omnibus_subdomain_dns_record" {
#   managed_zone = google_dns_managed_zone.dns_zone.name
#   name         = "app1."google_dns_managed_zone.dns_zone.dns_name
#   type         = "A"
#   rrdatas      = ["1.2.3.4"]
#   ttl          = 300
# }

# -----------------------------------------------------------------------------
# Add custom DNS records above this line
# -----------------------------------------------------------------------------
